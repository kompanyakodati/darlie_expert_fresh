<?php
include_once('inc/tab_config.inc.php');
include_once('inc/tab_header.inc.php');
?>

<script src="js/html5media.min.js"></script>       

<div id="container"  class="content_bg">

    
<div class="video_link" >

<!--[if IE]>
    <style type='text/css'>
        #videobox {
            margin-top:40px !important;
			margin-left:-60px !important;			
        }
    </style>
<![endif]--> 

<![if !IE]>
    <style type='text/css'>
        #videobox {
			margin-top: 58px !important;
			margin-left: -84px !important;
        }
    </style>  
<![endif]>


 
 
 <div id="videobox">
    <iframe width="400" height="300" src="//www.youtube.com/embed/PWIlCjAlT_8?autoplay=1&cc_load_policy=1" frameborder="0" allowfullscreen></iframe>     
 </div>
</div>
    
<ul class="ul_btns">
    <li><a href="#" class="share" onclick="shareFeed();return false;" ></a></li>
    <!-- <li><a href="#" class="invite" onclick="inviteFriends();return false;" ></a></li> -->
</ul>


    

<a href="#" class="bottom_link" onclick="bottomLink();return false;" >www.darlie.com.my</a>
    

  </div>    
    
    

    
    
    
</div>
<script type="text/javascript">
    
 function shareFeed() {
    FB.ui({
      method: 'feed',
      link: '<?=TAB_APP_URL?>',
      caption : 'Trust the Experts for Brilliant Results',
      picture : '<?=BASE_URL?>/images/thumbnail_200x200.png',
      description: 'Check out how the new Darlie ExPERT FRESH works to give you longer-lasting fresh breath - stay protected against bad breath for 12 hours!',
    }, function(response){        
        JU.GA.setGoogleEventTracker('<?=GOGLE_ANALYTICS_ACCOUNT?>','Button Clicks','Click on share button','Share Button');         
    });     
     
 }
 
 
 function inviteFriends() {
    FB.ui({method: 'apprequests',
      message: 'Show your friends the way to 3x whiter teeth with Darlie Expert White'
    }, function(){
        JU.GA.setGoogleEventTracker('<?=GOGLE_ANALYTICS_ACCOUNT?>','Button Clicks','Click on Invite Friends button','Invite Friends Button');                 
    }); 
    }
    
function bottomLink() {
    JU.GA.setGoogleEventTracker('<?=GOGLE_ANALYTICS_ACCOUNT?>','Button Clicks','Click on Link at the bottom','Bottom Link');
    window.open('http://www.darlie.com.my/en/products/toothpaste-detail.aspx?product=darlie-expert-white','_blank');
}



</script>




<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', '<?=GOGLE_ANALYTICS_ACCOUNT?>']);
_gaq.push(['_trackPageview']);
(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; 

ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';

var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>


<script src="<?=LIB_URL?>ju/ju.js" type="text/javascript"></script>	
<script src="<?=LIB_URL?>ju/ju.ga.js" type="text/javascript"></script>	
<script type="text/javascript" src="//www.youtube.com/iframe_api"></script>
<script type="text/javascript">
var videoArray = new Array();
var playerArray = new Array();    
(function($) {
	function trackYouTube()
	{		
		var i = 0;
		jQuery('iframe').each(function() {
			var video = $(this);
			if(video.attr('src')===undefined){
				var vidSrc = "";
			}else{
				var vidSrc = video.attr('src');
			}

			var regex = /h?t?t?p?s?\:?\/\/www\.youtube\.com\/embed\/([\w-]{11})(?:\?.*)?/;

			var matches = vidSrc.match(regex);
			if(matches && matches.length > 1){
					videoArray[i] = matches[1];
					$(this).attr('id', matches[1]);
					i++;			
			}
		});	
	}
	$(document).ready(function() {
		trackYouTube();
	});
})(jQuery);

function onPlayerReady(event) {
	//event.target.playVideo();
}

function onYouTubeIframeAPIReady() {
	//Now the battle is engaged
	//we sweep through our array and reference the
	//beating hearts of the youtube id's
	//We then create a new holy object into our
	//second array, by referencing each beating
	//youtube id heart, and tell it wence it
	//shall act upon events
	for (var i = 0; i < videoArray.length; i++) {
		playerArray[i] = new YT.Player(videoArray[i], {
			events: {
			'onReady': onPlayerReady,
			'onStateChange': onPlayerStateChange
			}
		});		
	}
}
var _pauseFlag = false;

function onPlayerStateChange(event) { 
	videoarraynum = event.target.id - 1;
	if (event.data ==YT.PlayerState.PLAYING){
                JU.GA.setGoogleEventTracker('<?=GOGLE_ANALYTICS_ACCOUNT?>','Video Clicks','Click on Play button','Play Button',videoArray[videoarraynum]);                 
		_pauseFlag = false;
	} 

	if (event.data ==YT.PlayerState.ENDED){
                JU.GA.setGoogleEventTracker('<?=GOGLE_ANALYTICS_ACCOUNT?>','Video Clicks','Watch to End','Play Button',videoArray[videoarraynum]);                                 
	} 
        
	if (event.data ==YT.PlayerState.PAUSED && _pauseFlag == false){
                JU.GA.setGoogleEventTracker('<?=GOGLE_ANALYTICS_ACCOUNT?>','Video Clicks','Video Paused','Play Button',videoArray[videoarraynum]);                                                 
		_pauseFlag = true;
	}
/*
        if (event.data ==YT.PlayerState.BUFFERING){
		ga('send', 'event', 'Videos', 'Buffering', videoArray[videoarraynum]);
	}
	if (event.data ==YT.PlayerState.CUED){
		ga('send', 'event', 'Videos', 'Cueing', videoArray[videoarraynum]);
	} 
*/        
} 


</script>
</body>
</html>