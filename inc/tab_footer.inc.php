<div id="footer_box">
	
		<a href="#" class="btn_terms"></a>
	
</div>

<div id="tncBox" style="display:none;">
		<ol>
			<li>This contest is open only to all Malaysian, age 18 and above. <br />
				<p>
					ELIGIBILITY : KOS&#201; (M) Sdn Bhd (the "Organizer")'s  "Black is the  New White" Contest (the "Contest") is only  
					open to individual Malaysians with valid NRIC who are currently residing in Malaysia. Employees of Organizer
					and Administrator (both defined below), and their respective parent companies, affiliates,subsidiaries, officers,
					directors, employees, contractors, representatives and agents of Orgnizer, and each of their respective 
					immediate families and household members (collectively the "Contest Entities") are not eligible to enter this 
					Contest.
				</p>			
			</li>
			<li>All participants must "Like" KOS&#201; Facebook Fan Page in order to be eligible to join this contest.</li>
			<li>All fields in the regitration form must be completed in order to be eligible.Incomplete forms will not be
				entertained and the participant will be automatically disqualified.
			</li>
			<li>
				Participants agree to release their personal details to KOS&#201; (M) Sdn Bhd to be processed, shared and 
				utilised for the purpose of registration for this contest.Such personal information may be used by KOS&#201; (M)
				Sdn Bhd to confirm the participant's identity, mailing address and contact number, to verify the eligibility of the
				participant for this contest and to send any promotional material on products and services of KOS&#201; (M) Sdn Bhd.
				Participants have the right to access, comment, ammend and/or cancel any of the personal information which has
				been recieved by KOS&#201; (M) Sdn Bhd by posting a private message on Facebook or sending an email to
				<u>enquiries@kose.com.my</u>. All personal information provided by participants will be used exclusively by
				KOS&#201; (M) Sdn Bhd and/or its associates or partners for the purpose as mentioned in these TERMS & CONDITIONS.
			</li>
			<li>
				This contest is not organised, sponsored, verified, controlled or associated with Facebook. All information submitted
				is to KOS&#201; (M) Sdn Bhd and not to Facebook.
			</li>
			<li>
				By participating in the contest, the winners agree to be interviewed and/or photographed and/or video graphed
				by KOS&#201; (M) Sdn Bhd.(at the discretion of KOS&#201; (M) Sdn Bhd)<br />
				<p>
					By entering this Contest, Entrant agrees that Organizer and its designees may use Entrat's,physical and/or
					email address to contact Entrant regarding Organizer's programming and other offers and/or promotions
					that Organizer believes may interest Entrant. The Organizer has the right to use the names and/or photographs
					of the Entrants and all other Entries submitted for advertising, trade and/or publicity purposes without any prior
					notice to the Entrants. The Entrants shall not be entitled to claim ownership or other forms of
					compensation on the materials.
				</p>
			</li>
			<li>	
				Every photo uploaed must feature people using Sekkisei Clear Whitening Mask. Any photo that does not have
				a person in it will be disqualified.
			</li>
			<li>A total of 6 winners will win Total prizes worth RM10,000.</li>
			<li>Winners will be announced via this application and/or KOS&#201; website at <u>www.kose.com.my</u>
				after the contest ends on 22<sup>nd</sup> September 2013.
			</li>
			<li>Each person can only participate ONCE,despite the number of accounts she uses to submit entries.</li>
			<li>Winner's selections are based on 40% public votes and 60% KOS&#201; (M) Sdn Bhd - Each Facebook user
			is not  allowed to vote for her photo.
			</li>
			<li>
				KOS&#201; (M) Sdn Bhd reserves the right at any time to replace the prizes with another of similar value.
			</li>
			<li>
				To redeem prizes, all winners are required to present an official receipt from any Parkson department
				store in Malaysia as a proof of purchase.
			</li>
		</ol>
</div>

<script type="text/javascript">

$(".btn_terms").click(function(e) {
	var _tnc = $("#tncBox").html();
	
	bootbox.alert( _tnc );
});

</script>
