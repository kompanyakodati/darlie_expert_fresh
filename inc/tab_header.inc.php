<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head >
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title><?=APP_TAB_NAME?></title>
	<script src="<?=LIB_URL?>jquery/jquery1.8.js" type="text/javascript"></script>
	<link href="<?=LIB_URL?>bootstrap/css/bootstrap.min.css" media="all" rel="stylesheet" type="text/css" />		
	<script src="<?=LIB_URL?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?=LIB_URL?>bootstrap/js/bootbox.min.js" type="text/javascript"></script>
	<link href="css/style.css?v=<?=time()?>" media="all" rel="stylesheet" type="text/css" />
	
	<script type="text/javascript">
	  window.fbAsyncInit = function() {
			 FB.init({
				  appId      : '<?=APP_FBID?>', 
				  channelUrl : '<?=PROTOCOL.$_SERVER['HTTP_HOST']."/"?>channel.html', // Channel File
				  status     : true,
				  cookie     : true,
				  xfbml      : true,
				  oauth  : true			  
			});
			FB.Canvas.setAutoGrow(100);
			FB.Canvas.scrollTo(0,0);		



	  };
	(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=<?=APP_FBID?>";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script>	
</head>
<body style="margin:0;padding:0;overflow: hidden;">	
<div id="fb-root"></div>