<?php
/**
	Author: Xerxis Anthony B. Alvar
*/

//SSL
define('PROTOCOL',( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://');

/* Settings to be Modified */
define('ENABLE_DEBUG_MODE',false);

// DATABASE ACCOUNT
define('DB_USERNAME','kosephotocontest');
define('DB_PASSWORD','jF3S70It4yNAxJy');
define('DB_HOST','localhost');
define('DB_NAME','kose_photo_contest_my');


// Default TimeZone
define('TIMEZONE' , 'Asia/Kuala_Lumpur'); //Malaysia
//define('TIMEZONE' , 'Asia/Singapore'); //Singapore


// APP SETTINGS
define('APP_DIRECTORYNAME', 'darlie_expert_fresh_tab');
define('APP_FBID','216702481873628');
define('APP_SECRET','836d4eddca721dabd4767d8c3df132f0');
define('APP_CANVASNAME','expertfresh');
define('APP_TAB_NAME',"ExPERT FRESH");
define('APP_FAN_PAGE_NAME', "Darlie Malaysia"); 
define('APP_PERMS','email');

// FAN PAGE SETTINGS
//define('FAN_PAGE_URL','www.facebook.com/DarlieMalaysia'); 
//define('FAN_PAGE_ID','101434503653');

define('FAN_PAGE_URL','www.facebook.com/Pocca.Media');
define('FAN_PAGE_ID','523759554359243');

//Canvas URL
define('CANVAS_URL', PROTOCOL.'apps.facebook.com/'.APP_CANVASNAME);


//Tab Application Settings
define('TAB_APP_URL', PROTOCOL . FAN_PAGE_URL . '/app_' . APP_FBID);


//GOOGLE ANALYTICS
define('GOGLE_ANALYTICS_ACCOUNT', 'UA-45194202-5'); 


$mobile_agent = '/(iPad|iPod|iPhone|Android|BlackBerry|SymbianOS|SCH-M\d+|Opera Mini|Windows CE|Nokia|SonyEricsson|webOS|PalmOS)/';
define('MOBILE',( preg_match($mobile_agent, $_SERVER['HTTP_USER_AGENT'])) ? true:false);


//SERVER RELATED
if (ENABLE_DEBUG_MODE) {	error_reporting(E_ALL);ini_set('display_errors', 1);ini_set('display_startup_errors', 1);} 
else {  error_reporting(0);	ini_set('display_errors', 0);ini_set('display_startup_errors',0);}

//Set PHP date/time to SG
date_default_timezone_set(TIMEZONE);
ini_set('default_charset', 'UTF-8');



//Paths  
//<<<---------------------- Update this details on LIVE -----------------------------------------------------------------------------------------
if (!defined('DIRECTORY_NAME')) define('DIRECTORY_NAME', APP_DIRECTORYNAME);
if (!defined('ABSPATH')) define('ABSPATH', $_SERVER["DOCUMENT_ROOT"]."/".DIRECTORY_NAME."/");
if (!defined('LIBPATH')) define('LIBPATH', $_SERVER["DOCUMENT_ROOT"]."/_tools/");
if (!defined('BASE_URL')) define('BASE_URL',PROTOCOL.$_SERVER['HTTP_HOST']."/".DIRECTORY_NAME.'/');
if (!defined('LIB_URL')) define('LIB_URL',PROTOCOL.$_SERVER['HTTP_HOST'].'/_tools/');



//AdoPhp Library
include_once(LIBPATH . 'adodb/adodb.inc.php');
include_once(LIBPATH . 'adodb/adodb-active-record.inc.php');

$ADODB_ASSOC_CASE = 2; //camelCase support for fieldname
//Support Transaction
$db = NewADOConnection("mysqlt://".DB_USERNAME.":".DB_PASSWORD."@".DB_HOST."/".DB_NAME);
//Supports other languages to be stored on db as is
//$db->Execute("set names 'utf8'"); 

// how to SET COLLATION when storing other language string
//$db->Execute("SET NAMES $charset COLLATE $collation");

//Admin Tool
define('ADMIN_USERNAME','admin');
define('ADMIN_PASSWORD','kose007');


//Google Analytics
define('GA_ACCOUNT',GOGLE_ANALYTICS_ACCOUNT);



//INCLUDE FILES
//Facebook PHP SDK
include_once(LIBPATH . 'facebook/facebook.php');
$facebook = new Facebook(array(
'appId' => APP_FBID,
'secret'=> APP_SECRET,
'cookie'=> false //Assumed $_REQUEST['signed_request'] will always come 
));

//Parsed Signed Request
if ( isset($_REQUEST['signed_request']) ) {

	function parse_signed_request($signed_request) {
	  list($encoded_sig, $payload) = explode('.', $signed_request, 2); 
	  // decode the data
	  $sig = base64_url_decode($encoded_sig);
	  $data = json_decode(base64_url_decode($payload), true);
	  return $data;
	}
	function base64_url_decode($input) {   return base64_decode(strtr($input, '-_', '+/')); }
	
	$signedRequest = parse_signed_request( $_REQUEST['signed_request'] );	
}
/*
//APP API
include_once(ABSPATH . 'apimanager.inc.php');
$api = new photocontest();

require_once(ABSPATH . 'adminapimanager.inc.php');
$adminapi = new photocontestAdmin();
*/
?>